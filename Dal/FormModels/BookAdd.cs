﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.FormModels
{
    public class BookAdd
    {
        public string Title { get; set; }

        public DateTime? DateOfPublish { get; set; }

        public List<int> CategoryIds { get; set; }

        public List<int> AuthorIds { get; set; }
    }
}
