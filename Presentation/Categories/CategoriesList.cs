﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dal.Models;
using Services;
using FormModels = Dal.FormModels;

namespace Presentation.Categories
{
    public partial class CategoriesList : Form
    {
        private readonly CategoriesService _service;
        public CategoriesList()
        {
            InitializeComponent();
            _service = new CategoriesService();
            FillGrid();
        }

        public void FillGrid()
        {
            var categories = _service.GetCategoriesNew();
            dgvCategories.Rows.Clear();
            foreach (var category in categories)
            {
                var rowIndex = dgvCategories.Rows.Add();
                dgvCategories.Rows[rowIndex].Cells["Id"].Value = category.Id;
                dgvCategories.Rows[rowIndex].Cells["CategoryName"].Value = category.Name;
            }

        }

        private void BtnNewCategory_Click(object sender, EventArgs e)
        {
            CategorySave categorySaveForm = new CategorySave();
            categorySaveForm.FormClosed += CategorySaveForm_FormClosed;
            categorySaveForm.ShowDialog();
            
        }

        private void CategorySaveForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            FillGrid();
        }

        private void DgvCategories_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                //update
                var categoryId = Convert.ToInt32(((DataGridView)sender).Rows[e.RowIndex].Cells["Id"].Value);
                var categoryName = dgvCategories.Rows[e.RowIndex].Cells["CategoryName"].Value.ToString();

                var category = new FormModels.Category(categoryId, categoryName);

                CategorySave categorySaveForm = new CategorySave(category);
                categorySaveForm.FormClosed += CategorySaveForm_FormClosed;
                categorySaveForm.ShowDialog();
            }

            if (e.ColumnIndex == 3)
            {
                //delete
                var mboxResult = MessageBox.Show("Are you sure want to delete this record?", "Delete record", MessageBoxButtons.YesNo);
                if (mboxResult == DialogResult.Yes)
                {
                    var categoryId = Convert.ToInt32(((DataGridView)sender).Rows[e.RowIndex].Cells["Id"].Value);

                    _service.DeleteCategory(categoryId);
                    MessageBox.Show("Record is deleted");
                    FillGrid();

                }
            }
        }
    }
}
